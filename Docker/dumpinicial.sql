/*
Navicat MySQL Data Transfer

Source Server         : COMT
Source Server Version : 50639
Source Host           : 172.16.100.8:3306
Source Database       : scan

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2021-02-23 16:24:30
*/
USE scan
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Alumnos
-- ----------------------------
DROP TABLE IF EXISTS `Alumnos`;
CREATE TABLE `Alumnos` (
  `Nombre` varchar(30) DEFAULT NULL,
  `Apellido` varchar(30) DEFAULT NULL,
  `Dni` int(8) DEFAULT NULL,
  `ECivil` int(1) DEFAULT NULL,
  `Domicilio` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Alumnos
-- ----------------------------
INSERT INTO `Alumnos` VALUES ('Matias', 'Corbalaán', '32655196', '0', 'apalalero');
INSERT INTO `Alumnos` VALUES ('Nazareno', 'Flach', '46665514', '0', 'Calle verdadera 321');
INSERT INTO `Alumnos` VALUES ('Guadalupe', 'Bher', '54345234', '0', 'Av. Siempre Viva 3333');
INSERT INTO `Alumnos` VALUES ('Pardu', 'ino', '34567892', '0', 'there');
INSERT INTO `Alumnos` VALUES ('Prueba', 'ApePrueba', '32322196', '0', 'Por ahi');
INSERT INTO `Alumnos` VALUES ('Matias222222', 'Corbalan22222', '31655197', '1', 'apalalero');
/*
Navicat MySQL Data Transfer

Source Server         : COMT
Source Server Version : 50639
Source Host           : 172.16.100.8:3306
Source Database       : scan

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2021-02-23 16:27:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Notas
-- ----------------------------
DROP TABLE IF EXISTS `Notas`;
CREATE TABLE `Notas` (
  `Dni` int(8) DEFAULT NULL,
  `mate` int(3) DEFAULT NULL,
  `histo` int(3) DEFAULT NULL,
  `len` int(3) DEFAULT NULL,
  `gym` int(3) DEFAULT NULL,
  `id` int(30) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Notas
-- ----------------------------
INSERT INTO `Notas` VALUES ('32655196', '8', '7', '7', null, '1');
INSERT INTO `Notas` VALUES ('32655196', '7', '7', '7', null, '2');
INSERT INTO `Notas` VALUES ('32655196', '8', '8', '8', null, '3');
INSERT INTO `Notas` VALUES ('32655196', '9', '9', '9', null, '4');
INSERT INTO `Notas` VALUES ('32655196', '10', '10', '10', null, '5');
INSERT INTO `Notas` VALUES ('46665514', '4', '10', '10', '10', '6');
INSERT INTO `Notas` VALUES ('46665514', '10', '7', '7', null, '7');
INSERT INTO `Notas` VALUES ('46665514', '10', '10', '10', '10', '8');
INSERT INTO `Notas` VALUES ('32655196', '10', '10', '10', '10', '10');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '1', '1', '11');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '1', '1', '12');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '1', '1', '13');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '6', '1', '14');
