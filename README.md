# Evaluacion - TECO
## _Aplicativo de testing para evaluacion_

[![M|Corbalan]()](https://cvonline.me/@matiascorbalan)

Evaluacion planteada para crear un desarrollo de un portal de alumnos para un colegio secundario se debera mostrar:

- ✨Datos del alumno
- ✨Nota de las materias cursadas


## Features

- Lenguaje utilizado para Backend Python
- Librerias usadas [Flask, Flask_mysql]
- Apis rest para consultas y edicion de datos



Como entorno de trabajo se levanto el servicio en un Centos 7, el mismo cuenta con lo siguiente:

> Python 3.6.8
> Flask 1.1.2
> Werkzeug 1.0.1
> Server version: 5.6.39 MySQL
> pip 9.0.3

## Instalacion

Clonar Repositorio:

```sh
git clone https://gitlab.com/Mathias32/secundaria.git
cd secundaria/Docker
docker-compose up -d
mysql -uroot -p -h 127.0.0.1 < dumpinicial.sql
```

Con estos pasos ya esta listo para usar, solicitara una clave para mysql se manda por mail. Luego deben entrar a:

```sh
http://0.0.0.0:8081
```

## Enlaces

- [Python] - Sitio oficial.
- [Flask] - Site de oficial con repositorios

Repositorio Git [Gitlab][dill].


## Testing

Se realizaron casos de testing que permiten el ingreso y modificacion de Alumnos y notas.

| Proceso | Resultado |
| ------ | ------ |
| Alta de Usuario | Se genero el formulario de alta, se crearon varios registros sin problemas |
| Modificar Usuarios [WEB] | Se probo modificar a los usuarios sin inconvenientes |
| Modificar Usuarios [API] | Se generon Api de modificacion suando DNI como Key|
| Listar alumnos [WEB]| Se logro listar el total de alumnos sin inconveniente |
| Listar alumnos [API] | Se genero Api que devuelve el listado de Alumnos |
| Listar Notas [WEB] | Se logro listar las ultimas 4 notas de cada alumno |
| Listar Notas [API] | Se genero Api para listar las notas por alumno |

## Datos

> Las apis devuelven un Json con el listado predefinido
> Cada Alta o modificacion redirige al Home
> Se crearon en laces para las distitnas funcionalidades

Path de las API's.

Actualizacion de usuarios:
>Nombre-> Matias
>Apellido->Corbalan
>Dni->32655196(Campo Clave)
>ECivil-> Estado civil 1=Casado 0=Soltero
>Domicilio-> Av Santa Fe 1234
```sh
http://localhost:8081/API_updateAlumno?Nombre=Matias&Apellido=Corbalan&Dni=32655196&ECivil=0&Domicilio=Av Santa Fe 1234
```

Alta de usuario:
>Nombre-> Matias
>Apellido->Corbalan
>Dni->32655196(Campo Clave)
>ECivil-> Estado civil 1=Casado 0=Soltero
>Domicilio-> Av Santa Fe 1234

```sh
http://localhost:8081/API_insertAlumno?Nombre=Matias&Apellido=Corbalan&Dni=32655196&ECivil=1&Domicilio=Av Santa Fe 1234
```

Listado de Alumnos:

```sh
http://localhost:8081/GetAlumnos
```

Notas por alumno:
```sh
http://localhost:8081/NotaxAlumno/32655196
```

#### Levantar API

Linux:

```sh
sudo python3 index.py
```

Parametros de configuracion en index:
>LocalHost
>Usuario
>Clave
>Base
```sh
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'p1p0l0c0'
app.config['MYSQL_DB'] = 'scan'
```

## Mysql 

Creacion de tabla Alumnos e insercion de campos

```sh
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Alumnos
-- ----------------------------
DROP TABLE IF EXISTS `Alumnos`;
CREATE TABLE `Alumnos` (
  `Nombre` varchar(30) DEFAULT NULL,
  `Apellido` varchar(30) DEFAULT NULL,
  `Dni` int(8) DEFAULT NULL,
  `ECivil` int(1) DEFAULT NULL,
  `Domicilio` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
```sh
-- ----------------------------
-- Records of Alumnos
-- ----------------------------
INSERT INTO `Alumnos` VALUES ('Matias', 'Corbalaán', '32655196', '0', 'apalalero');
INSERT INTO `Alumnos` VALUES ('Nazareno', 'Flach', '46665514', '0', 'Calle verdadera 321');
INSERT INTO `Alumnos` VALUES ('Guadalupe', 'Bher', '54345234', '0', 'Av. Siempre Viva 3333');
INSERT INTO `Alumnos` VALUES ('Pardu', 'ino', '34567892', '0', 'there');
INSERT INTO `Alumnos` VALUES ('Prueba', 'ApePrueba', '32322196', '0', 'Por ahi');
INSERT INTO `Alumnos` VALUES ('Matias222222', 'Corbalan22222', '31655197', '1', 'apalalero');.
```

Creacion de tabla Notas e insercion de campos:

```sh
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Notas
-- ----------------------------
DROP TABLE IF EXISTS `Notas`;
CREATE TABLE `Notas` (
  `Dni` int(8) DEFAULT NULL,
  `mate` int(3) DEFAULT NULL,
  `histo` int(3) DEFAULT NULL,
  `len` int(3) DEFAULT NULL,
  `gym` int(3) DEFAULT NULL,
  `id` int(30) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

```

```sh
-- ----------------------------
-- Records of Notas
-- ----------------------------
INSERT INTO `Notas` VALUES ('32655196', '8', '7', '7', null, '1');
INSERT INTO `Notas` VALUES ('32655196', '7', '7', '7', null, '2');
INSERT INTO `Notas` VALUES ('32655196', '8', '8', '8', null, '3');
INSERT INTO `Notas` VALUES ('32655196', '9', '9', '9', null, '4');
INSERT INTO `Notas` VALUES ('32655196', '10', '10', '10', null, '5');
INSERT INTO `Notas` VALUES ('46665514', '4', '10', '10', '10', '6');
INSERT INTO `Notas` VALUES ('46665514', '10', '7', '7', null, '7');
INSERT INTO `Notas` VALUES ('46665514', '10', '10', '10', '10', '8');
INSERT INTO `Notas` VALUES ('32655196', '10', '10', '10', '10', '10');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '1', '1', '11');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '1', '1', '12');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '1', '1', '13');
INSERT INTO `Notas` VALUES ('32655196', '1', '1', '6', '1', '14');
```
##DOCKER



## License

Matias corbalan 
[Linkedin](https://www.linkedin.com/in/matias-corbalan-63476539/)
[CV-Online](https://cvonline.me/@matiascorbalan)



   [dill]: <https://gitlab.com/Mathias32>
   [Flask]: <https://flask.palletsprojects.com/en/1.1.x/>
   
