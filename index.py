     ## @package Api Secundaria 
     # Evaluacion ingreso TECO.
     #
     # @matiascorbalan
     # @version 1.1
     # @date 23/02/2021
     #
     # el desarrollo permite listar crear y modificar alumnos y sus notas
from flask import Flask ,jsonify ,make_response, redirect
from flask import render_template
from flask import request
from flask_mysqldb import MySQL



app = Flask(__name__)

# Configuracion de DB
#  \_Completar los datos con los correspondientes para cada DB

app.config['MYSQL_HOST'] = 'mysql'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'p1p0l0c0'
app.config['MYSQL_DB'] = 'scan'

mysql = MySQL(app)
@app.route('/')
def home():
    ## Pagina inicial de alumnos.
    #
    # @param No recibe parametros 
    # @return Devuelve un listado de los alumnos registrados y sus datos
    # personales
  
    cur = mysql.connection.cursor()
    resultado = cur.execute("SELECT * from  Alumnos")
    if resultado > 0:
        ListadoAlumnos = cur.fetchall()
        return render_template('Alumnos.html', LAlumnos=ListadoAlumnos)
    
@app.route('/altaAlumno')
def AltaAlumno():
    ## Redirect para crear alumnos.
    #
    # @param No recibe parametros 
    # @return Redirect
    return render_template('altaAlumno.html')

@app.route('/cargarNotas')
def cargarNotas():
    ## Cargar las notas .
    #
    # @param No recibe parametros 
    # @return listado de alumno para formulario 
    cur = mysql.connection.cursor()
    resultado = cur.execute("SELECT * from  Alumnos")
    if resultado > 0:
        ListadoAlumnos = cur.fetchall()
        return render_template('cargarNotas.html',LAlumnos=ListadoAlumnos)
    
@app.route('/insertNota', methods=['GET','POST'])
def insertNota():
    ## Funcion para crar registro de nota por aluno
    #
    # @param recibe parametros por POST[Dni,mate,histo,len,gym]
    # @return Genera el registro y redirige al Home
    if request.method =="POST":
        formulario = request.form
        DNI = formulario["Dni"]
        mate = formulario['mate']
        histo = formulario['histo']
        leng = formulario['len']
        gym = formulario['gym']
        cur = mysql.connection.cursor()
        update = cur.execute("""
        INSERT INTO Notas 
        (Dni,
        mate,
        histo,
        len,
        gym) 
        VALUES (
        '"""+DNI+"""',
        '"""+mate+"""',
        '"""+histo+"""',
        '"""+leng+"""',
        '"""+gym+"""')
        """)
        mysql.connection.commit()
    return redirect('/')

@app.route('/insertAlumno', methods=['GET','POST'])
def insertAlumno():
    ## Crea registro de Alumno.
    #
    # @param Recibe parametros por POST[Dni,Nombre,Apellido,ECivil,Domicilio] 
    # @return Genera registro y redirige al Home
    if request.method =="POST":
        formulario = request.form
        DNI = formulario["Dni"]
        Nombre = formulario['Nombre']
        Apellido = formulario['Apellido']
        ECivil = formulario['ECivil']
        Domicilio = formulario['Domicilio']
        cur = mysql.connection.cursor()
        update = cur.execute("""
        INSERT INTO Alumnos 
        (Nombre,
        Apellido,
        Dni,
        ECivil,
        Domicilio) 
        VALUES (
        '"""+Nombre+"""',
        '"""+Apellido+"""',
        '"""+DNI+"""',
        '"""+ECivil+"""',
        '"""+Domicilio+"""')
        """)
        mysql.connection.commit()
    return redirect('/')

@app.route('/infoAlumno', methods=['GET', 'POST'])
def infoAlumno():
    ## Muestra las ultimas 4 notas registradas.
    #
    # @param Recibe el DNI para poder indexar la busqueda
    # @return Genera el listado de las ultimas 4 notas guardadas
    cur = mysql.connection.cursor()
    DNI = request.args.get('Dni')
    Nam = cur.execute("SELECT Nombre, Apellido FROM Alumnos WHERE Dni ='" + DNI + "'")
    nombre = cur.fetchall()
    cur.execute(""" SELECT *
    FROM Notas
    WHERE Dni = '""" + DNI + """'
    ORDER BY id DESC
    LIMIT 4""")
    Notas = cur.fetchall()
    return render_template('infoAlumno.html', Dni=DNI, nombre=nombre[0][0], apellido=nombre[0][1], notas=Notas)

@app.route('/updateAlumno', methods=['GET','POST'])
def updateAlumno():
    ## Modifica los valores de los alumnos.
    #
    # @param Recibe los parametros por POST[Dni,Nombre,Apellido,ECivil,Domicilio] 
    # @return Actualiza los campos que fueron modificados en el formulario,
    # utiliza como key value el DNI
    if request.method =="POST":
        formulario = request.form
        DNI = formulario["Dni"]
        Nombre = formulario['Nombre']
        Apellido = formulario['Apellido']
        ECivil = formulario['ECivil']
        Domicilio = formulario['Domicilio']
        cur = mysql.connection.cursor()
        Insert = cur.execute("""
        UPDATE Alumnos 
        SET Nombre='"""+Nombre+"""',
        Apellido='"""+Apellido+"""',
        ECivil='"""+ECivil+"""',
        Domicilio='"""+Domicilio+"""'
        WHERE
        Dni='"""+DNI+"""'
        LIMIT 1
        """)
        mysql.connection.commit()
    return redirect('/')

@app.route('/editAlumno', methods=['GET', 'POST'])
def editAlumno():
    ## Recibe la informacion del alumno que va a ser editado.
    #
    # @param Recibe DNI
    # @return Devuelve informacion del alumno correspondiente
    cur = mysql.connection.cursor()
    DNI = request.args.get('Dni')
    Nam = cur.execute("SELECT * FROM Alumnos WHERE Dni ='" + DNI + "'")
    info = cur.fetchall()
    return render_template('editAlumno.html', Dni=DNI, info=info)

@app.route('/GetAlumnos')
def GetAlumnos():
    ## API.
    #
    # @param No recibe parametros 
    # @return Devuelve el listado de todos los alumnos con formato JSON
    cur = mysql.connection.cursor()
    resultado = cur.execute("SELECT * from  Alumnos")
    ListadoAlumnos = cur.fetchall()
    Respuesta = make_response(jsonify(ListadoAlumnos))
    Respuesta.headers["Content-Type"] = "application/json"
    return Respuesta

@app.route('/NotaxAlumno/<DNI>')
def GetNotaAlumno(DNI):
    ## API.
    #
    # @param Recibe el DNI 
    # @return Devuelve un listado con las ultimas 4 notas del DNI indicado
    cur = mysql.connection.cursor()
    resultado = cur.execute("""SELECT *
    FROM Notas
    WHERE Dni = '""" + DNI + """'
    ORDER BY id DESC
    LIMIT 4 """)
    ListadoAlumnos = cur.fetchall()
    Respuesta = make_response(jsonify(ListadoAlumnos))
    Respuesta.headers["Content-Type"] = "application/json"
    return Respuesta

@app.route('/API_updateAlumno', methods=['GET', 'POST'])
def APIUpdateAlumno():
    ## API.
    #
    # @param recibe parametros por POST[Nombre, Apellido,ECivil,Domicilio,Dni] 
    # @return Actualiza los valores del alumno usando como Key value el DNI
    if request.method=='GET':
        Nombre  = request.args.get('Nombre')
        Apellido  = request.args.get('Apellido')
        ECivil  = request.args.get('ECivil')
        Domicilio  = request.args.get('Domicilio')
        Dni  = request.args.get('Dni')
        
        cur = mysql.connection.cursor()
        update = cur.execute("""
        UPDATE Alumnos 
        SET Nombre='"""+Nombre+"""',
        Apellido='"""+Apellido+"""',
        ECivil='"""+ECivil+"""',
        Domicilio='"""+Domicilio+"""'
        WHERE
        Dni='"""+Dni+"""'
        LIMIT 1
        """)
        mysql.connection.commit()
        
        resultado = cur.execute("SELECT * from  Alumnos WHERE Dni ='"+Dni+"'")
        ListadoAlumnos = cur.fetchall()
        Respuesta = make_response(jsonify(ListadoAlumnos))
        Respuesta.headers["Content-Type"] = "application/json"
    return Respuesta

@app.route('/API_insertAlumno', methods=['GET', 'POST'])
def APIInsertAlumno():
    ## API
    #
    # @param Recibe los parametros por GET[Nombre, Apellido,Ecivil,Domicilio,Dni]
    # @return Genera un registro en la BD dando de alta un nuevo Alumno
    if request.method=='GET':
        Nombre  = request.args.get('Nombre')
        Apellido  = request.args.get('Apellido')
        ECivil  = request.args.get('ECivil')
        Domicilio  = request.args.get('Domicilio')
        Dni  = request.args.get('Dni')
        
        cur = mysql.connection.cursor()
        Insert = cur.execute("""
        INSERT INTO Alumnos 
        (Nombre,
        Apellido,
        Dni,
        ECivil,
        Domicilio) 
        VALUES (
        '"""+Nombre+"""',
        '"""+Apellido+"""',
        '"""+Dni+"""',
        '"""+ECivil+"""',
        '"""+Domicilio+"""')
        """)
        mysql.connection.commit()
        
        resultado = cur.execute("SELECT * from  Alumnos WHERE Dni ='"+Dni+"'")
        ListadoAlumnos = cur.fetchall()
        Respuesta = make_response(jsonify(ListadoAlumnos))
        Respuesta.headers["Content-Type"] = "application/json"
    return Respuesta
  
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081,debug=True)
    #\_Se configuro el puero 8081 ya que el 80 esta usado para desarrollo
